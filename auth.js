const jwt = require('jsonwebtoken')
//jsonwebtoken ay para ma cover natin yung user ng another layer of security
//meron syang header, payload and secret

const secret = 'YOINK'

//Create a Token

module.exports.createAccessToken = (user) => {
    console.log(user);

    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }

    return jwt.sign(data, secret, {})
}

//Token Verification

module.exports.verify = (req, res, next) => {
    console.log(req.headers.authorization)

    let token = req.headers.authorization;

    //use an if statement to check the jwt token
    if (typeof token === "undefined") {
        return res.send({ auth: "Failed. No Token" })
    } else {
        console.log(token);
        token = token.slice(7, token.length)

        jwt.verify(token, secret, function (err, decodedToken) {
            //err contains error from decoding the token
            if (err) {
                return res.send({
                    auth: "Failed",
                    message: err.message
                })
            } else {
                console.log(decodedToken)//contains data from our token

                req.user = decodedToken;
                //user property

                next();
            }
        })
    }
}
//verification for admin, can also be used for middleware

module.exports.verifyAdmin = (req, res, next) => {
    if (req.user.isAdmin) {
        next();
    } else {
        return res.send({
            auth: "Failed",
            message: "Action Forbidden"
        })
    }
}
//[SECTION] Modules
const express = require('express')
const mongoose = require('mongoose')
const { required } = require('nodemon/lib/config')

//[SECTION] Schema/Blueprint
const userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: [true, 'Email is Required']
    },
    password: {
        type: String,
        required: [true, 'Password is Required']
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    orderUser: [
        {
            products: {
                productId: { type: String, required: true },
                productName: { type: String, required: true },
                price: { type: Number, required: true },
                quantity: { type: String, required: true }
            },
            totalAmount: {
                type: Number,
                required: true
            },
            purchasedOn: {
                type: Date,
                default: new Date()
            }
        }]

})



//[SECTION] Model
module.exports = mongoose.model('User', userSchema);

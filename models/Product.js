//[SECTION] Dependencies and Modules
const mongoose = require('mongoose')
//[SECTION] Schema/Blueprint
const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    price: {
        type: Number,
        required: [true, 'Product Price is Required']
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    orders: [
        {
            orderId: {
                type: String,
                required: [true, 'Error in products,orderid']
            },
            placedOn: {
                type: Date,
                default: new Date()
            }
        }]
})
//[SECTION] Model
module.exports = mongoose.model('Product', productSchema)
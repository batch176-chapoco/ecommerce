//[SECTION] Dependencies and Modules
const User = require('../models/User')
const Product = require('../models/Product')
const bcrypt = require('bcrypt');
const dotenv = require('dotenv');
const auth = require('../auth.js')
//[SECTION] Environment Variables Setup
dotenv.config();
const asin = Number(process.env.salt);
//[SECTION] USER REGISTRATION
module.exports.register = (userData) => {
    let email = userData.email;
    let passW = userData.password;

    let newUser = new User({
        email: email,
        password: bcrypt.hashSync(passW, asin),
    });

    return newUser.save().then((user, err) => {

        if (user) {
            return user;
        } else {
            return 'Failed to Register'
        }
    })
};
//[SECTION] USER AUTHENTICATION
module.exports.loginUser = (data) => {
    return User.findOne({ email: data.email }).then(result => {
        if (result == null) {
            return false;
        } else {

            const isPasswordCorrect = bcrypt.compareSync(data.password, result.password)

            if (isPasswordCorrect) {
                return { accessToken: auth.createAccessToken(result.toObject()) }
            } else {
                return false;
            }
        }
    })
}

//Create order for a Registered User
module.exports.order = async (req, res) => {
    console.log(req.user.id)//from decoded token
    console.log(req.body.productId)//from postman body

    //for non admin user only
    if (req.user.isAdmin) {
        return res.send({ message: "Action Forbidden" })
    }

    let isUserUpdated = await User.findById(req.user.id).then(user => {
        let newOrder = {
            products: { productId: req.body.productId, productName: req.body.productName, price: req.body.price, quantity: req.body.quantity },
            totalAmount: (req.body.price * req.body.quantity)
        }

        user.orderUser.push(newOrder);

        return user.save().then(user => true).catch(err => err.message)

        if (isUserUpdated !== true) {
            return res.send({ message: isUserUpdated })
        }

    });

    let isProductUpdated = await Product.findById(req.body.productId).then(product => {

        let orderedProduct = {
            orderId: req.user.id
        }

        product.orders.push(orderedProduct)

        return product.save().then(course => true).catch(err => err.message)

        if (isProductUpdated !== true) {
            return res.send({ message: isProductUpdated })
        }
    })

    if (isUserUpdated && isProductUpdated) {
        return res.send({ message: "Order Placed Succesfully" })
    }
}

//Retrieve User Details

module.exports.getProfile = (data) => {
    return User.findById(data).then(result => {
        //change the value of the user's password to an empty string
        result.password = '';
        return result;
    })
}

//Update user to Admin
module.exports.updateUser = (userId) => {
    let updateAdminField = {
        isAdmin: true
    };

    return User.findByIdAndUpdate(userId, updateAdminField).then((user, error) => {
        if (error) {
            return false;
        } else {
            return true;
        }
    }).catch(error => error)
}

//Retrieve all product id in users
module.exports.userOrders = () => {
    return User.find().then(result => {
        return result
    }).catch(error => error)
}



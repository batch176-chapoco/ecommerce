//[SECTION] Dependencies and Modules
const Product = require('../models/Product')
const bcrypt = require('bcrypt');
const { trusted } = require('mongoose');

//[SECTION] Environment Variables Setup
//Create a Product
module.exports.productreg = (productData) => {
    let name = productData.name;
    let description = productData.description;
    let price = productData.price;

    let newProduct = new Product({
        name: name,
        description: description,
        price: price
    });
    return newProduct.save().then((user, err) => {

        if (user) {
            return user;
        } else {
            return 'Failed to register Product'
        }
    })
};

//Retrieve all Active Products
module.exports.activeProducts = () => {
    return Product.find({}).then(result => {
        return result
    }).catch(error => error)
};

//Retrieve a Specific Product
module.exports.oneProduct = (reqParams) => {
    return Product.findById(reqParams).then(result => {
        return result
    }).catch(error => error)
}

//Update a Product by Id (ADMIN)
module.exports.updateProduct = (productId, data) => {
    let updatedProduct = {
        name: data.name,
        description: data.description,
        price: data.price
    }

    return Product.findByIdAndUpdate(productId, updatedProduct).then((product, error) => {
        if (error) {
            return false;
        } else {
            return true;
        }
    }).catch(error => error)

}

//Archiving a Product
module.exports.archiveProduct = (productId) => {
    let updateActiveField = {
        isActive: false
    };

    return Product.findByIdAndUpdate(productId, updateActiveField).then((product, error) => {
        if (error) {
            return false;
        } else {
            return true;
        }
    }).catch(error => error)
}

//Activating a product
module.exports.activateProduct = (productId) => {
    let updateActiveField = {
        isActive: true
    };

    return Product.findByIdAndUpdate(productId, updateActiveField)
        .then((product, error) => {
            if (error) {
                return false;
            } else {
                return true;
            }
        }).catch(error => error)
}
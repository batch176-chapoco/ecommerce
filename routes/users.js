//[SECTION] Dependencies and Modules
const exp = require('express')
const controller = require('../controller/users')
const auth = require('../auth')
//[SECTION] Routing Component
const route = exp.Router();

//[SECTION] USER REGISTRATION
route.post('/register', (req, res) => {
    console.log(req.body);
    let userData = (req.body);
    controller.register(userData).then(outcome => {
        res.send(outcome)
    })
});

//[SECTION] USER AUTHENTICATION
route.post('/login', (req, res) => {
    controller.loginUser(req.body).then(result => res.send(result));
});

//CREATE ORDER
route.post('/order', auth.verify, controller.order)

//Get User Details
route.get('/details', auth.verify, (req, res) => {
    controller.getProfile(req.user.id).then(result => res.send(result))
})


//Route for Updating a Specific User
route.put('/:userId', auth.verify, auth.verifyAdmin, (req, res) => {
    controller.updateUser(req.params.userId, req.body).then
        (outcome => res.send(outcome))
});


//Route for Getting all productID
route.get('/allProductID', (req, res) => {
    controller.userOrders().then(outcome => {
        res.send(outcome)
    })
})





module.exports = route;
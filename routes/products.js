const exp = require('express')
const ProductsController = require('../controller/products')
const route = exp.Router();
const auth = require('../auth');

//destructuring authentication function
const { verify, verifyAdmin } = auth;

//Route for Creating a Product
route.post('/productreg', verify, verifyAdmin, (req, res) => {
    console.log(req.body);
    ProductsController.productreg(req.body).then(outcome => {
        res.send(outcome)
    })
});

//Route for Getting all Active Products
route.get('/activeProducts', (req, res) => {
    ProductsController.activeProducts().then(outcome => {
        res.send(outcome)
    })
});

//Route for Getting a Specific Product
route.get('/:productId', (req, res) => {
    console.log(req.params.productId)
    ProductsController.oneProduct(req.params.productId).then(outcome => {
        res.send(outcome)
    })
});

//Route for Updating a Specific Product
route.put('/:productId', verify, verifyAdmin, (req, res) => {
    ProductsController.updateProduct(req.params.productId, req.body).then
        (outcome => res.send(outcome))
});


//Route for Archiving a Product
route.put('/:productId/archive', verify, verifyAdmin, (req, res) => {
    ProductsController.archiveProduct(req.params.productId).then(outcome => res.send(outcome))
});

//Route to Activate a course
route.put('/:productId/activate', verify, verifyAdmin, (req, res) => {
    ProductsController.activateProduct(req.params.productId)
        .then(result => res.send(result));
})





module.exports = route;